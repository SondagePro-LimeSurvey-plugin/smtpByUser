<?php
/**
 * Personal settings edition
 */
?>

    <h3 class="pagetitle"><?php eT("Email settings"); ?></h3>
<div class="form-group row">
    <label class="col-sm-2 control-label"  for="smtpByUser_emailsmtphost"><?php eT("SMTP host:"); ?></label>
    <div class="col-sm-3">
        <?php
            echo \CHtml::textField("smtpByUser[emailsmtphost]",$emailsmtphost,
                array("class"=>"form-control","size"=>'50')
            );
        ?>
    </div>
    <div class="col-sm-3">
        <div class="text-info"><?php printf(gT("Enter your hostname and port, e.g.: %s"),"smtp.example.org:25"); ?></div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2  control-label"  for='smtpByUser_emailsmtpuser'><?php eT("SMTP username:"); ?></label>
    <div class="col-sm-3">
        <?php
            echo \CHtml::textField("smtpByUser[emailsmtpuser]",$emailsmtpuser,
                array("class"=>"form-control","size"=>'50')
            );
        ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2  control-label"  for='smtpByUser_emailsmtppassword'><?php eT("SMTP password:"); ?></label>
    <div class="col-sm-3">
        <?php
            echo \CHtml::passwordField("smtpByUser[emailsmtppassword]","",
                array("class"=>"form-control","size"=>'50',"autocomplete"=>"off",'placeholder'=>$emailsmtppassword_placeholder)
            );
        ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2  control-label"  for='smtpByUser_emailsmtpssl'><?php eT("SMTP encryption:"); ?></label>
    <div class="col-sm-3">
        <?php $this->widget('yiiwheels.widgets.buttongroup.WhButtonGroup', array(
            'name' => 'smtpByUser[emailsmtpssl]',
            'value'=> $emailsmtpssl ,
            'selectOptions'=>array(
                ""=>gT("Off",'unescaped'),
                "ssl"=>gT("SSL",'unescaped'),
                "tls"=>gT("TLS",'unescaped')
            )
        ));?>
    </div>
</div>
